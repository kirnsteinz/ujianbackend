<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\UnitRumah;

class ProductController extends Controller
{   
    function GetUnit(){
        try{ 
            $unitlist = DB::table('unit')->get();
            return response()->json($unitlist, 200);
        }
        catch(\Exception $e){
            return response()->json(["message"=>""+$e->getMessage()], 500);
        }
    }
    function CreateUnit(Request $request){
        DB::beginTransaction();
        try{          
            $unit = new UnitRumah;
            $unit->kavling = $request->input('kavling');
            $unit->blok = $request->input('blok');
            $unit->no_rumah = $request->input('no_rumah');
            $unit->harga_rumah = $request->input('harga_rumah');
            $unit->luas_tanah = $request->input('luas_tanah');
            $unit->luas_bangunan = $request->input('luas_bangunan');
            $unit->customer_id = $request->input('customer_id');
            $unit->save();  

            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>""+$e->getMessage()], 500);
        }   
    }
    function DeleteUnit(Request $request){
        DB::beginTransaction();
        try{
            $val = $request->input('id');
            DB::delete('delete from unit where id = ?', [$val]);
            DB::commit();
            return response()->json(["message"=>"success"], 200);    
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>""+$e->getMessage()], 500);
        } 
           
    }
}
